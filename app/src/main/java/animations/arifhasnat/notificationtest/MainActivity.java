package animations.arifhasnat.notificationtest;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.TaskStackBuilder;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.app.NotificationCompat;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {
    Context context;

    Button button;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);




        button = (Button) findViewById(R.id.buttons);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //Create notification object and set the content.
                NotificationCompat.Builder nb= new NotificationCompat.Builder(MainActivity.this);
                nb.setSmallIcon(R.drawable.image);
                nb.setContentTitle("Set your title");
                nb.setContentText("Set Content text");
                nb.setTicker("Set Ticker text");


                //get the bitmap to show in notification bar
                Bitmap bitmap_image = BitmapFactory.decodeResource(MainActivity.this.getResources(), R.drawable.image);
                NotificationCompat.BigPictureStyle s = new NotificationCompat.BigPictureStyle().bigPicture(bitmap_image);
                s.setSummaryText("Summary text appears on expanding the notification");
                nb.setStyle(s);



                Intent resultIntent = new Intent(MainActivity.this, MainActivity.class);
                TaskStackBuilder TSB = TaskStackBuilder.create(MainActivity.this);
                TSB.addParentStack(MainActivity.class);
                // Adds the Intent that starts the Activity to the top of the stack
                TSB.addNextIntent(resultIntent);
                PendingIntent resultPendingIntent =
                        TSB.getPendingIntent(
                                0,
                                PendingIntent.FLAG_UPDATE_CURRENT
                        );

                nb.setContentIntent(resultPendingIntent);
                nb.setAutoCancel(true);
                NotificationManager mNotificationManager =
                        (NotificationManager) MainActivity.this.getSystemService(Context.NOTIFICATION_SERVICE);
                // mId allows you to update the notification later on.
                mNotificationManager.notify(11221, nb.build());
            }
        });
    }

}
